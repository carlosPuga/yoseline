require('dotenv').config();
const express = require('express');
const app = express();

const port = process.env.PORT;
var hbs = require('hbs');

//Handelbars
hbs.registerPartials(__dirname + '/views/partials', function (err) {});
app.set('view engine', 'hbs');

app.use( express.static('public'));



app.get('/', function (req, res) {
    res.send("Hello World");
})

app.get('/*', function (req, res) {
    res.sendFile(  __dirname + '/public/index');
})


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});